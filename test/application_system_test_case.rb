require 'test_helper'
Capybara.register_driver :selenium_chrome_headless_docker_friendly do |app|
  Capybara::Selenium::Driver.load_selenium
  browser_options = ::Selenium::WebDriver::Chrome::Options.new
  browser_options.args << '--headless'
  browser_options.args << '--disable-gpu'
  # Sandbox cannot be used inside unprivileged Docker container
  browser_options.args << '--no-sandbox'
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
end

Capybara.javascript_driver = :selenium_chrome_headless_docker_friendly
class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, screen_size: [1400, 1400]
  setup do
    Capybara.current_driver = Capybara.javascript_driver # :selenium by default
  end
end
